package com.digit88.app.model;

import java.util.ArrayList;

public class Movie {
    private String poster_path;


    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }
}

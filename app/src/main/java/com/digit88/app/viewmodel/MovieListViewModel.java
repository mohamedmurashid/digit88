package com.digit88.app.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.digit88.app.model.MovieResult;
import com.digit88.app.model.Resource;
import com.digit88.app.repository.MovieListRepository;


public class MovieListViewModel extends ViewModel {
    private MutableLiveData<Resource<MovieResult>> mNowPlayingMovie;

    public MovieListViewModel() {
        MovieListRepository movieListRepository = new MovieListRepository();
        mNowPlayingMovie =  movieListRepository.getNowPlayingMovies();
    }

    public MutableLiveData<Resource<MovieResult>> getNowplayingMovie() {
        return mNowPlayingMovie;
    }

}
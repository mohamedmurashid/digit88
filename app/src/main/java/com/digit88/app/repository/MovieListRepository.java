package com.digit88.app.repository;

import android.util.Log;

import androidx.lifecycle.MutableLiveData;

import com.digit88.app.model.MovieResult;
import com.digit88.app.model.Resource;
import com.digit88.app.util.Application;
import com.digit88.app.util.Constant;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieListRepository {

    public MutableLiveData<Resource<MovieResult>> getNowPlayingMovies() {
        final MutableLiveData<Resource<MovieResult>> moviewResult = new MutableLiveData<>();

        Call<MovieResult> call  = Application.getInstance().getNetworkService()
                .getPopularMovie( Constant.SERVER_API_KEY);

        call.enqueue(new Callback<MovieResult>() {
            @Override
            public void onResponse(Call<MovieResult> call, Response<MovieResult> response) {
                MovieResult body = response.body();
                Log.d(Constant.TAG, "onResponse: popular moview "+body);
                Log.d(Constant.TAG, "onResponse: popular Url "+response.raw().request().url());

                if (body != null) {
                    moviewResult.setValue(Resource.success(body));
                } else {
                    moviewResult.setValue(Resource.<MovieResult>error("No Data", null));
                }
            }
            @Override
            public void onFailure(Call<MovieResult> call, Throwable t) {
                moviewResult.setValue(Resource.<MovieResult>error(t.getMessage(),null));
            }
        });
        return moviewResult;
    }


}

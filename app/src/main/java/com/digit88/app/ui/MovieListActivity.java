package com.digit88.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.digit88.app.adapter.NowPlayingMovieAdapter;
import com.digit88.app.R;
import com.digit88.app.model.Movie;
import com.digit88.app.viewmodel.MovieListViewModel;

import java.util.ArrayList;

public class MovieListActivity extends AppCompatActivity  {

    private ArrayList<Movie> mNowPlayingMovie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);

        init();
    }

    private void init() {
        MovieListViewModel mViewModel = new ViewModelProvider(this).get(MovieListViewModel.class);

        mNowPlayingMovie = new ArrayList<>();
        NowPlayingMovieAdapter nowPlayingMovieAdapter = new NowPlayingMovieAdapter(this, mNowPlayingMovie);
        RecyclerView rv_now_playing_movies = findViewById(R.id.rv_now_playing_movies);
        rv_now_playing_movies.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
        rv_now_playing_movies.setAdapter(nowPlayingMovieAdapter);

        mViewModel.getNowplayingMovie().observe(this, nowPlayingMovieResource -> {
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            switch (nowPlayingMovieResource.status)
            {
                case SUCCESS:
                    if (nowPlayingMovieResource.data != null) {
                        mNowPlayingMovie.addAll(nowPlayingMovieResource.data.getResults());
                        nowPlayingMovieAdapter.notifyDataSetChanged();
                    }
                    break;

                case ERROR:
                    if (nowPlayingMovieResource.data != null) {
                        Toast.makeText(this, nowPlayingMovieResource.data.toString(),Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        });
    }
}
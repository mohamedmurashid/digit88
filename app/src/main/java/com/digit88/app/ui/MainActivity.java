package com.digit88.app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.digit88.app.R;
import com.digit88.app.adapter.HorizontalAdapter;
import com.digit88.app.util.Constant;
import com.digit88.app.util.Util;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArrayList<String> mItems = new ArrayList<String>();
        mItems.addAll(Util.getTenItems());
         HorizontalAdapter horizontalAdapter = new HorizontalAdapter(this, mItems);
        RecyclerView rv_horizontal = findViewById(R.id.rv_horizontal);
        rv_horizontal.setAdapter(horizontalAdapter);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        rv_horizontal.setLayoutManager(linearLayoutManager);
        rv_horizontal.addOnScrollListener (new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_DRAGGING  && linearLayoutManager.findLastVisibleItemPosition() == mItems.size()-1) {
                    Log.d(Constant.TAG, "onScrollStateChanged:  SCROLL_STATE_DRAGGING "+ linearLayoutManager.findLastVisibleItemPosition());
                    mItems.addAll(Util.getTenItems());
                    horizontalAdapter.notifyDataSetChanged();
                }
            }
        });

        findViewById(R.id.tv_next).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, MovieListActivity.class));
            }
        });


    }
}
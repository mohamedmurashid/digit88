package com.digit88.app.network;



import com.digit88.app.model.MovieResult;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NetworkService {

    @GET("3/movie/popular")
    Call<MovieResult> getPopularMovie(@Query("api_key") String apiKey);


}